<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return redirect('home/landingpage');
});
Route::get("home", function() {return redirect('home/landingpage');});
Route::get("home/landingpage", function() {return view("landingpage");});
Route::get("home/location", function() {return view("location");});

Route::get("home/towns", function() {return view("towns");});
Route::post('home/towns', 'App\Http\Controllers\GetTownsController@read');

Route::get("home/newtown", function() {return view("newtown");});
Route::post('home/newtown', 'App\Http\Controllers\NewTown@create');

Route::get("home/contact", function() {return view("contact");});
Route::post('home/contact', 'App\Http\Controllers\ContactFormController@create');
Route::get("home/messages", 'App\Http\Controllers\MessagesController@read');

use App\Http\Controllers\GalleryController;
Route::resource('gallery',GalleryController::class);

use App\Http\Controllers\PostController;
Route::resource('posts', PostController::class);
