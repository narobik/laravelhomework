<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ContactForm;
class MessagesController extends Controller
{
    public function read(Request $request)
    {
        $tomb=ContactForm::orderBy('id', 'DESC')->get();
        $messages =array('msg'=>$tomb);
		return view('messages',$messages);

        
    }
}
