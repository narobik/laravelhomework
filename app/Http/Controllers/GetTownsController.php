<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class GetTownsController extends Controller
{
    public function read(Request $request)
    {
		$towns = DB::table('towns')->where('county_id', "=" , $request['countyid_form'])->get();
        return view('towns', compact('towns'));
    }


    
}
