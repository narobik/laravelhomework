<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Town;
class NewTown extends Controller
{
    public function create(Request $request)
    {
        $tomb=array('tname' =>$request->tname,'county_id' =>$request->county_id,'countyseat' =>$request->countyseat,'countylevel' =>$request->countylevel);
        Town::create($tomb);
        $msg=array('mes'=>"Creation is done.");
        return view('newtown', $msg);
    }
}
