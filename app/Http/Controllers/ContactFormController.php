<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ContactForm;
class ContactFormController extends Controller
{
    public function create(Request $request)
    {
        // Print form content, but first validate the Form data:
    $this->validate($request,[
    'from'=>'required|min:0|max:20',
    'message'=>'required|min:10|max:255',
    ]);
        //$tomb=array('tname' =>$request->tname,'county_id' =>$request->county_id,'countyseat' =>$request->countyseat,'countylevel' =>$request->countylevel);
        ContactForm::create($request->all());
        $msg=array('mes'=>"Message saved.");
        return view('contact', $msg);
    }
}
