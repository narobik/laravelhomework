<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Town extends Model
{
    protected $fillable = ['tname','county_id','countyseat', 'countylevel']; 
    use HasFactory;

    public function county()
    {
        return $this->belongsTo(County::class);
    }

    public function population()
    {
        return $this->hasMany(Population::class);
    }
}
