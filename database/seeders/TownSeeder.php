<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class TownSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
DB::table('towns')->insert(['tname' => 'Mindszent','county_id' => 8,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Komádi','county_id' => 16,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Tiszacsege','county_id' => 16,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Nyíradony','county_id' => 16,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Rákóczifalva','county_id' => 15,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Mezőkeresztes','county_id' => 3,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Nagyatád','county_id' => 4,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Tatabánya','county_id' => 17,'countyseat' => -1, 'countylevel' => -1
]);
DB::table('towns')->insert(['tname' => 'Tótkomlós','county_id' => 1,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Lenti','county_id' => 9,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Körösladány','county_id' => 1,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Barcs','county_id' => 4,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Harkány','county_id' => 12,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Sülysáp','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Tura','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Pilisvörösvár','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Mohács','county_id' => 12,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Budapest 16','county_id' => 10,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Százhalombatta','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Nyíregyháza','county_id' => 20,'countyseat' => -1, 'countylevel' => -1
]);
DB::table('towns')->insert(['tname' => 'Gödöllő','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Veszprém','county_id' => 18,'countyseat' => -1, 'countylevel' => -1
]);
DB::table('towns')->insert(['tname' => 'Kiskőrös','county_id' => 11,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Balatonföldvár','county_id' => 4,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Martfű','county_id' => 15,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Mezőhegyes','county_id' => 1,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Medgyesegyháza','county_id' => 1,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Törökszentmiklós','county_id' => 15,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Pomáz','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Fertőd','county_id' => 19,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Dunaharaszti','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Putnok','county_id' => 3,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Komló','county_id' => 12,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Dunavecse','county_id' => 11,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Tab','county_id' => 4,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Baktalórántháza','county_id' => 20,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Hajdúhadház','county_id' => 16,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Pálháza','county_id' => 3,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Vásárosnamény','county_id' => 20,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Göd','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Kisvárda','county_id' => 20,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Budapest 21','county_id' => 10,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Piliscsaba','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Ajak','county_id' => 20,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Püspökladány','county_id' => 16,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Érd','county_id' => 7,'countyseat' => 0, 'countylevel' => -1
]);
DB::table('towns')->insert(['tname' => 'Biharkeresztes','county_id' => 16,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Pilis','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Budapest 15','county_id' => 10,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Kemecse','county_id' => 20,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Hódmezővásárhely','county_id' => 8,'countyseat' => 0, 'countylevel' => -1
]);
DB::table('towns')->insert(['tname' => 'Hajdúböszörmény','county_id' => 16,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Velence','county_id' => 5,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Salgótarján','county_id' => 14,'countyseat' => -1, 'countylevel' => -1
]);
DB::table('towns')->insert(['tname' => 'Kecel','county_id' => 11,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Szécsény','county_id' => 14,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Budapest 01','county_id' => 10,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Budapest 18','county_id' => 10,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Budapest 10','county_id' => 10,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Sellye','county_id' => 12,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Budakalász','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Dévaványa','county_id' => 1,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Körmend','county_id' => 6,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Zamárdi','county_id' => 4,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Kenderes','county_id' => 15,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Budapest 12','county_id' => 10,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Cegléd','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Kondoros','county_id' => 1,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Csepreg','county_id' => 6,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Fót','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Hajdúszoboszló','county_id' => 16,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Őrbottyán','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Nyírlugos','county_id' => 20,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Újszász','county_id' => 15,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Keszthely','county_id' => 9,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Lajosmizse','county_id' => 11,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Elek','county_id' => 1,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Ajka','county_id' => 18,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Ibrány','county_id' => 20,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Budapest 19','county_id' => 10,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Aszód','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Zalalövő','county_id' => 9,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Dunaújváros','county_id' => 5,'countyseat' => 0, 'countylevel' => -1
]);
DB::table('towns')->insert(['tname' => 'Baja','county_id' => 11,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Siófok','county_id' => 4,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Tiszafüred','county_id' => 15,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Bácsalmás','county_id' => 11,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Bátonyterenye','county_id' => 14,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Balatonkenese','county_id' => 18,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Újhartyán','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Ráckeve','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Budapest 17','county_id' => 10,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Zsámbék','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Zirc','county_id' => 18,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Záhony','county_id' => 20,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Berhida','county_id' => 18,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Halásztelek','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Hatvan','county_id' => 2,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Vámospércs','county_id' => 16,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Adony','county_id' => 5,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Kerepes','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Fertőszentmiklós','county_id' => 19,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Tokaj','county_id' => 3,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Hajós','county_id' => 11,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Mezőtúr','county_id' => 15,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Mándok','county_id' => 20,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Esztergom','county_id' => 17,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Veresegyház','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Berettyóújfalu','county_id' => 16,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Polgár','county_id' => 16,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Hajdúsámson','county_id' => 16,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Szeghalom','county_id' => 1,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Tolna','county_id' => 13,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Balmazújváros','county_id' => 16,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Sárbogárd','county_id' => 5,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Bonyhád','county_id' => 13,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Abádszalók','county_id' => 15,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Gyöngyöspata','county_id' => 2,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Mezőcsát','county_id' => 3,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Balatonalmádi','county_id' => 18,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Karcag','county_id' => 15,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Szarvas','county_id' => 1,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Marcali','county_id' => 4,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Aba','county_id' => 5,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Budapest 09','county_id' => 10,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Enying','county_id' => 5,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Dabas','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Csanádpalota','county_id' => 8,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Budapest 04','county_id' => 10,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Vecsés','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Bóly','county_id' => 12,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Örkény','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Zalaszentgrót','county_id' => 9,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Kunhegyes','county_id' => 15,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Tamási','county_id' => 13,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Ócsa','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Tiszalök','county_id' => 20,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Solt','county_id' => 11,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Komárom','county_id' => 17,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Nyírtelek','county_id' => 20,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Kőszeg','county_id' => 6,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Demecser','county_id' => 20,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Kiskunmajsa','county_id' => 11,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Máriapócs','county_id' => 20,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Miskolc','county_id' => 3,'countyseat' => -1, 'countylevel' => -1
]);
DB::table('towns')->insert(['tname' => 'Nyergesújfalu','county_id' => 17,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Mezőberény','county_id' => 1,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Kistelek','county_id' => 8,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Edelény','county_id' => 3,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Ózd','county_id' => 3,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Izsák','county_id' => 11,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Nyékládháza','county_id' => 3,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Dunakeszi','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Csongrád','county_id' => 8,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Pécs','county_id' => 12,'countyseat' => -1, 'countylevel' => -1
]);
DB::table('towns')->insert(['tname' => 'Szentgotthárd','county_id' => 6,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Jászfényszaru','county_id' => 15,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Sásd','county_id' => 12,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Újfehértó','county_id' => 20,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Mezőkovácsháza','county_id' => 1,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Visegrád','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Balatonfüred','county_id' => 18,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Tiszakécske','county_id' => 11,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Balassagyarmat','county_id' => 14,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Szigethalom','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Rudabánya','county_id' => 3,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Fonyód','county_id' => 4,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Gyönk','county_id' => 13,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Tököl','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Balatonfűzfő','county_id' => 18,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Túrkeve','county_id' => 15,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Vác','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Igal','county_id' => 4,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Kiskunfélegyháza','county_id' => 11,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Soltvadkert','county_id' => 11,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Budapest 23','county_id' => 10,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Tiszavasvári','county_id' => 20,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Sarkad','county_id' => 1,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Hajdúnánás','county_id' => 16,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Gyomaendrőd','county_id' => 1,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Martonvásár','county_id' => 5,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Maglód','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Kisköre','county_id' => 2,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Kecskemét','county_id' => 11,'countyseat' => -1, 'countylevel' => -1
]);
DB::table('towns')->insert(['tname' => 'Cigánd','county_id' => 3,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Simontornya','county_id' => 13,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Orosháza','county_id' => 1,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Diósd','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Tata','county_id' => 17,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Fehérgyarmat','county_id' => 20,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Szentes','county_id' => 8,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Mór','county_id' => 5,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Sopron','county_id' => 19,'countyseat' => 0, 'countylevel' => -1
]);
DB::table('towns')->insert(['tname' => 'Kunszentmiklós','county_id' => 11,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Mélykút','county_id' => 11,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Szabadszállás','county_id' => 11,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Makó','county_id' => 8,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Sárospatak','county_id' => 3,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Kaba','county_id' => 16,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Debrecen','county_id' => 16,'countyseat' => -1, 'countylevel' => -1
]);
DB::table('towns')->insert(['tname' => 'Csorvás','county_id' => 1,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Kazincbarcika','county_id' => 3,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Rétság','county_id' => 14,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Balkány','county_id' => 20,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Sümeg','county_id' => 18,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Celldömölk','county_id' => 6,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Bátaszék','county_id' => 13,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Gyömrő','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Kiskunhalas','county_id' => 11,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Budapest 05','county_id' => 10,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Szentendre','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Csenger','county_id' => 20,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Polgárdi','county_id' => 5,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Szendrő','county_id' => 3,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Herend','county_id' => 18,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Devecser','county_id' => 18,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Gárdony','county_id' => 5,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Lébény','county_id' => 19,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Isaszeg','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Zalaegerszeg','county_id' => 9,'countyseat' => -1, 'countylevel' => -1
]);
DB::table('towns')->insert(['tname' => 'Vasvár','county_id' => 6,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Nagymaros','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Pannonhalma','county_id' => 19,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Beled','county_id' => 19,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Vép','county_id' => 6,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Hévíz','county_id' => 9,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Tiszaföldvár','county_id' => 15,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Dunaföldvár','county_id' => 13,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Mezőkövesd','county_id' => 3,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Újkígyós','county_id' => 1,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Fegyvernek','county_id' => 15,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Sajószentpéter','county_id' => 3,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Gyöngyös','county_id' => 2,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Szekszárd','county_id' => 13,'countyseat' => -1, 'countylevel' => -1
]);
DB::table('towns')->insert(['tname' => 'Abony','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Budapest 14','county_id' => 10,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Budaörs','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Gönc','county_id' => 3,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Dorog','county_id' => 17,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Tét','county_id' => 19,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Jánossomorja','county_id' => 19,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Hajdúdorog','county_id' => 16,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Alsózsolca','county_id' => 3,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Gyál','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Badacsonytomaj','county_id' => 18,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Gyula','county_id' => 1,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Budapest 11','county_id' => 10,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Mosonmagyaróvár','county_id' => 19,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Nagykáta','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Felsőzsolca','county_id' => 3,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Nagyhalász','county_id' => 20,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Budapest 22','county_id' => 10,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Tát','county_id' => 17,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Budapest 20','county_id' => 10,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Bodajk','county_id' => 5,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Dombrád','county_id' => 20,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Budapest 08','county_id' => 10,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Budapest 07','county_id' => 10,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Kapuvár','county_id' => 19,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Csorna','county_id' => 19,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Nádudvar','county_id' => 16,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Győr','county_id' => 19,'countyseat' => -1, 'countylevel' => -1
]);
DB::table('towns')->insert(['tname' => 'Nagyecsed','county_id' => 20,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Nagykőrös','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Mágocs','county_id' => 12,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Tiszaújváros','county_id' => 3,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Paks','county_id' => 13,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Nagybajom','county_id' => 4,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Lábatlan','county_id' => 17,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Kaposvár','county_id' => 4,'countyseat' => -1, 'countylevel' => -1
]);
DB::table('towns')->insert(['tname' => 'Nyírmada','county_id' => 20,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Balatonlelle','county_id' => 4,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Rácalmás','county_id' => 5,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Szerencs','county_id' => 3,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Besenyszög','county_id' => 15,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Tápiószele','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Csurgó','county_id' => 4,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Nagykanizsa','county_id' => 9,'countyseat' => 0, 'countylevel' => -1
]);
DB::table('towns')->insert(['tname' => 'Rakamaz','county_id' => 20,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Biatorbágy','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Répcelak','county_id' => 6,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Szombathely','county_id' => 6,'countyseat' => -1, 'countylevel' => -1
]);
DB::table('towns')->insert(['tname' => 'Pápa','county_id' => 18,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Békéscsaba','county_id' => 1,'countyseat' => -1, 'countylevel' => -1
]);
DB::table('towns')->insert(['tname' => 'Sátoraljaújhely','county_id' => 3,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Albertirsa','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Jánoshalma','county_id' => 11,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Encs','county_id' => 3,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Budapest 06','county_id' => 10,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Lőrinci','county_id' => 2,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Szigetvár','county_id' => 12,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Szigetszentmiklós','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Jászapáti','county_id' => 15,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Sándorfalva','county_id' => 8,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Székesfehérvár','county_id' => 5,'countyseat' => -1, 'countylevel' => -1
]);
DB::table('towns')->insert(['tname' => 'Kisújszállás','county_id' => 15,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Szentlőrinc','county_id' => 12,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Nagykálló','county_id' => 20,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Jászkisér','county_id' => 15,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Mátészalka','county_id' => 20,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Derecske','county_id' => 16,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Bük','county_id' => 6,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Budapest 02','county_id' => 10,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Vaja','county_id' => 20,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Budapest 13','county_id' => 10,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Nagymányok','county_id' => 13,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Bábolna','county_id' => 17,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Dombóvár','county_id' => 13,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Kistarcsa','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Balatonboglár','county_id' => 4,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Kunszentmárton','county_id' => 15,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Őriszentpéter','county_id' => 6,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Battonya','county_id' => 1,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Tompa','county_id' => 11,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Lengyeltóti','county_id' => 4,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Ercsi','county_id' => 5,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Bélapátfalva','county_id' => 2,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Monor','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Siklós','county_id' => 12,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Nyírbátor','county_id' => 20,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Onga','county_id' => 3,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Pacsa','county_id' => 9,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Szeged','county_id' => 8,'countyseat' => -1, 'countylevel' => -1
]);
DB::table('towns')->insert(['tname' => 'Zalakaros','county_id' => 9,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Emőd','county_id' => 3,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Kisbér','county_id' => 17,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Letenye','county_id' => 9,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Borsodnádasd','county_id' => 3,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Kozármisleny','county_id' => 12,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Sajóbábony','county_id' => 3,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Kalocsa','county_id' => 11,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Dunavarsány','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Vésztő','county_id' => 1,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Pétervására','county_id' => 2,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Budakeszi','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Kadarkút','county_id' => 4,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Jászárokszállás','county_id' => 15,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Bicske','county_id' => 5,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Csákvár','county_id' => 5,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Pécel','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Jánosháza','county_id' => 6,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Villány','county_id' => 12,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Szolnok','county_id' => 15,'countyseat' => -1, 'countylevel' => -1
]);
DB::table('towns')->insert(['tname' => 'Budapest 03','county_id' => 10,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Abaújszántó','county_id' => 3,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Verpelét','county_id' => 2,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Kerekegyháza','county_id' => 11,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Szob','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Szikszó','county_id' => 3,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Békés','county_id' => 1,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Pusztaszabolcs','county_id' => 5,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Füzesgyarmat','county_id' => 1,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Törökbálint','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Pásztó','county_id' => 14,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Tapolca','county_id' => 18,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Üllő','county_id' => 7,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Ács','county_id' => 17,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Eger','county_id' => 2,'countyseat' => -1, 'countylevel' => -1
]);
DB::table('towns')->insert(['tname' => 'Mórahalom','county_id' => 8,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Sárvár','county_id' => 6,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Létavértes','county_id' => 16,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Heves','county_id' => 2,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Pécsvárad','county_id' => 12,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Várpalota','county_id' => 18,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Füzesabony','county_id' => 2,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Oroszlány','county_id' => 17,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Jászberény','county_id' => 15,'countyseat' => 0, 'countylevel' => 0
]);
DB::table('towns')->insert(['tname' => 'Téglás','county_id' => 16,'countyseat' => 0, 'countylevel' => 0
]);


    }
}
