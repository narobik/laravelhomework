<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class CountySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('counties')->insert(['cname' => 'Békés
        ']);
        DB::table('counties')->insert(['cname' => 'Heves
        ']);
        DB::table('counties')->insert(['cname' => 'Borsod-Abaúj-Zemplén
        ']);
        DB::table('counties')->insert(['cname' => 'Somogy
        ']);
        DB::table('counties')->insert(['cname' => 'Fejér
        ']);
        DB::table('counties')->insert(['cname' => 'Vas
        ']);
        DB::table('counties')->insert(['cname' => 'Pest
        ']);
        DB::table('counties')->insert(['cname' => 'Csongrád
        ']);
        DB::table('counties')->insert(['cname' => 'Zala
        ']);
        DB::table('counties')->insert(['cname' => 'Budapest
        ']);
        DB::table('counties')->insert(['cname' => 'Bács-Kiskun
        ']);
        DB::table('counties')->insert(['cname' => 'Baranya
        ']);
        DB::table('counties')->insert(['cname' => 'Tolna
        ']);
        DB::table('counties')->insert(['cname' => 'Nógrád
        ']);
        DB::table('counties')->insert(['cname' => 'Jász-Nagykun-Szolnok
        ']);
        DB::table('counties')->insert(['cname' => 'Hajdú-Bihar
        ']);
        DB::table('counties')->insert(['cname' => 'Komárom-Esztergom
        ']);
        DB::table('counties')->insert(['cname' => 'Veszprém
        ']);
        DB::table('counties')->insert(['cname' => 'Győr-Moson-Sopron
        ']);
        DB::table('counties')->insert(['cname' => 'Szabolcs-Szatmár-Bereg
        ']);
    }
}
