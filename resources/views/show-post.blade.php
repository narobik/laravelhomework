@extends("master")
@section("title") Show Post @endsection
@section("content")
<div class="container">
    <div class="row">
        <div class="col-xl-12 col-lg-12 text-right">
            <a href="{{route('posts.index')}}" class="btn btn-dark"> Back to Posts </a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 m-auto">
            <div class="card shadow">
                <div class="card-header">
                    <h4 class="card-title"> Show Post </h4>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="title"> Title </label>
                        <input type="text" readonly name="title" class="form-control" id="title" value="@if(!empty($post)) {{$post->title}} @endif">
                    </div>
                    <div class="form-group">
                        <label for="description"> Description </label>
                        <textarea class="form-control" readonly name="description" id="description">@if(!empty($post)) {{$post->description}} @endif</textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection