@extends("master")
@section("title") Posts @endsection
@section("content")
<div class="row mb-4">
<a href="/home" class="btn btn-dark">Back to home</a>
    <div class="col-xl-6">
        @if(Session::has('success'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert">× </button>
                {{Session::get('success')}}
            </div>
        @endif
        @if(Session::has('failed'))
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert">× </button>
                {{Session::get('failed')}}
            </div>
        @endif 
    </div>
    <div class="col-xl-6 text-right">
        <a href="{{route('posts.create')}}" class="btn btn-dark "> Add New </a>
    </div>
</div>
<table class="table table-striped">
    <thead>
        <th> Id </th>
        <th> Title </th>
        <th> Description </th>
        <th> Action </th>
    </thead>
    <tbody>
        @if(count($posts) > 0)
            @foreach($posts as $post)
                <tr>
                    <td> {{$post->id}} </td>
                    <td> {{$post->title}} </td>
                    <td> {{$post->description}} </td>
                    <td>
                        <form action="{{route('posts.destroy', $post->id)}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <a href="{{route('posts.show', $post->id)}}" class="btn btn-sm btn-secondary"> Show </a>
                            <a href="{{route('posts.edit', $post->id)}}" class="btn btn-sm btn-secondary"> Edit </a>
                            <button type="submit" class="btn btn-sm btn-secondary"> Delete </button>
                        </form>
                    </td>
                </tr>
            @endforeach
        @endif
    </tbody>
</table>
{!! $posts->links() !!}
@endsection