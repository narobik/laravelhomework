@extends('parent')
@section('main')
    @if($errors->any())
    <div class="alert alert-danger">
        <ul>
        @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
        </ul>
    </div>
    @endif
    <h4 align="center">Upload Image</h4>
    <div align="right">
        <a href="{{ route('gallery.index') }}" class="btn btn-dark">Back</a>
    </div>
    <form method="post" action="{{ route('gallery.store') }}" enctype="multipart/form-data">
        <div class="form-group">
            <label class="col-md-4 text-right">Title</label>
            <div>
                <input type="text" name="title" class="form-control input-lg" />
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-8">
                <input type="file" name="image" />
            </div>
        </div>
        <div class="form-group text-center">
            <input type="submit" name="add" class="btn btn-secondary input-lg" value="Add" />
        </div>
    </form>
@endsection