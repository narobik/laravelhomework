@extends("mainpage")
@section("content")
<div class="hero d-flex justify-content-center align-items-center">
  <div class="container text d-flex flex-column my-5">

    <form method="post" action="towns">
      @csrf 
      <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">Enter a County ID (1-20)</label>
        <input type="number" class="form-control" id="exampleInputEmail1" name="countyid_form" min=1 max=20>
      </div>

      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
    @if (isset($towns) > 0)
    <ul class="list-group my-5">
      @foreach ($towns as $town)
        <li class="list-group-item">{{$town->tname}}</li>
      @endforeach
    </ul>
    @endif  
  </div>
</div>
@stop