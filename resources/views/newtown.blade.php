@extends("mainpage")
@section("content")
<div class="hero d-flex justify-content-center align-items-center">
  <div class="container text d-flex flex-column my-5">

    <form method="post" action="newtown">
      @csrf 
      <div class="mb-3">
        <label for="exampleInput" class="form-label">Enter the Town name </label>
        <input type="text" class="form-control" id="exampleInput" name="tname">
        <label for="exampleInput2" class="form-label">Enter the County ID (1-20)</label>
        <input type="number" class="form-control" id="exampleInput2" name="county_id" min=1 max=20>
        <label for="exampleInput3" class="form-label">Is county seat?  (0 no-1 yes)</label>
        <input type="number" class="form-control" id="exampleInput3" name="countyseat" min=0 max=1>
        <label for="exampleInput4" class="form-label">Is county level? (0 no-1 yes)</label>
        <input type="number" class="form-control" id="exampleInput4" name="countylevel" min=0 max=1>
      </div>

      <button type="submit" class="btn btn-primary">Submit</button>
    </form> 

    @if (isset($mes))
    <h1 class="my-5">Successful addition</h1>
    @endif 
  </div>
</div>
@stop