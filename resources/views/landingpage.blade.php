@extends("mainpage")
@section("content")
<div class="hero d-flex justify-content-center align-items-center">
  <div class="container text d-flex flex-column my-5">
    <h1 class="text-center mb-3">Welcom to our website!</h1>

    <iframe id="video" src="https://www.youtube.com/embed/5ftGaUWPE1c" title="YouTube video player"
      frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
      allowfullscreen></iframe>
      <br>
    <video controls>
      <source src="/video.mp4" type="video/mp4">
      Your browser does not support the video tag.
    </video>
  </div>
</div>
@stop
