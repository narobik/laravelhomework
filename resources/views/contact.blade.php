@extends("mainpage")
@section("content")
<div class="hero d-flex justify-content-center align-items-center">
  <div class="container text d-flex flex-column my-5">

    <form method="post" action="contact">
      @csrf 
      @if (count($errors) > 0)
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
        @endif  
      <div class="mb-3">
        <label for="exampleInput" class="form-label">Your name </label>
        <input type="text" class="form-control" id="exampleInput" name="from" minlength=0 maxlength=20  required>
        <label for="exampleInput2" class="form-label">Message</label>
        <input type="text" class="form-control" id="exampleInput2" name="message" minlength=10 maxlength=255 required>
      </div>

      <button type="submit" class="btn btn-primary">Submit</button>
    </form> 

    @if (isset($mes))
    <h1 class="my-5">Successful addition</h1>
    @endif 
  </div>
</div>
@stop