@extends('parent')
@section('main')
    <h4 align="center">View Image</h4>
    <div class="jumbotron text-center">
        <div align="right">
            <a href="{{ route('gallery.index') }}" class="btn btn-dark">Back</a>
        </div>
        <br />
        <img src="{{ URL::to('/') }}/images/{{ $data->image }}" class="img-thumbnail" />
        <h3> {{ $data->title }} </h3>
    </div>
@endsection