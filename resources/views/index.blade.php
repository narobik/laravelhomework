@extends('parent')
@section('main')
<div align="right">
    <a href="{{ route('gallery.create') }}" class="btn btn-dark">Add +</a>
</div>
<div class="row">
    @foreach($images as $row)
        <div class="col-md-6 col-lg-4 col-xl-3">
          <a href="{{ route('gallery.show', $row->id) }}">
              <img src="{{ URL::to('/') }}/images/{{ $row->image }}"  style="width:100%" >
          </a>
          <div class="caption">
            <p align="center">{{ $row->title}}</p>
          </div>
          <form action="{{ URL::route('gallery.destroy',$row->id) }}" method="POST">
            <a href="{{ route('gallery.show', $row->id) }}" class="btn btn-secondary">View </a>
            <a href="{{ route('gallery.edit', $row->id) }}" class="btn btn-secondary">Edit </a>          
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <button class="btn btn-secondary">Delete </button>
          </form>                
        </div>
    @endforeach
</div>
{{ $images->links('pagination::bootstrap-4') }}
@endsection

