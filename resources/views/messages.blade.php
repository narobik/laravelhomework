@extends("mainpage")
@section("content")
<div class="hero d-flex justify-content-center align-items-center">
  <div class="container text d-flex flex-column my-5">

      @foreach ($msg as $m)
      <ul class="list-group my-5">
        <li class="list-group-item">From: {{$m->from}}</li>
        <li class="list-group-item">Message: {{$m->message}}</li>
     </ul>
      @endforeach
    
  </div>
</div>
@stop